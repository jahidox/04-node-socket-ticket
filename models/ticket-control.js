const path = require('path');
const fs = require('fs');

class Ticket {
  constructor(number, desktop) {
    this.number = number;
    this.desktop = desktop;
  }
}

class TicketControl {

    constructor() {
      this.last = 0;
      this.today = new Date().getDate();
      this.tickets = [];
      this.last_4 = [];

      this.init();
    }

    get toJSON() {
      return {
        last: this.last,
        today: this.today,
        tickets: this.tickets,
        last_4: this.last_4
      }
    }

    init() {
      const {last, today, tickets, last_4} = require('../db/data.json');
      if(today === this.today){
        this.tickets = tickets;
        this.last = last;
        this.last_4 = last_4;
      }else {
        this.saveDataBase();
      }
    }

    saveDataBase() {
      const db_path = path.join(__dirname, '../db/data.json');
      fs.writeFileSync(db_path, JSON.stringify(this.toJSON));
    }

    next() {
      this.last += 1;
      const ticket = new Ticket(this.last, null);
      this.tickets.push(ticket);
      this.saveDataBase();
      return `Ticket ${ticket.number}`;
    }

    attend_ticket(desktop) {
      if(this.tickets.length === 0) {
        return null;
      }

      const ticket = this.tickets.shift(); //this.tickets[0];
      ticket.desktop = desktop;
      this.last_4.unshift(ticket); //Añadir al principio

      if(this.last_4.length > 4) {
        this.last_4.splice(-1, 1); //Borrar el ultimo
      }

      this.saveDataBase();
      return ticket;
    }

}

module.exports = TicketControl;