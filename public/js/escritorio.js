console.log('Escritorio HTML');
const lblDesktop = document.querySelector('h1');
const btnAttend = document.querySelector('button');
const lblTicket = document.querySelector('small');
const alert = document.querySelector('.alert');
const alert_msg = document.querySelector('.alert_msg');
const lblPendientes = document.querySelector('#lblPendientes');

const searchParams = new URLSearchParams(window.location.search);
if (!searchParams.has('escritorio')){
    window.location = 'index.html';
    throw new Error('Escritorio  es obligatorio');
}

const desktop = searchParams.get('escritorio');
lblDesktop.innerText = 'Escritorio: ' + desktop;
alert.style.display = 'none';

const socket = io();

socket.on('connect', () => {
  btnAttend.disabled = false;
});

socket.on('disconnect', () => {
  btnAttend.disabled = true;
});

socket.on('last-ticket', (last) => {
  // lblDesktop.innerText = 'Escritorio: ' + desktop;
});

socket.on('pending-tickets', (pending) => {
  if (pending > 0) {
    alert.style.display = 'none';
  }
  lblPendientes.innerText = pending;
});

btnAttend.addEventListener( 'click', () => {
  socket.emit('attend-ticket', {desktop}, ({ok, ticket, msg}) => {
    if (!ok){
      lblTicket.innerText = '....'
      alert.style.display = '';
      return alert_msg.innerText = msg;
    }
    lblTicket.innerText = 'Ticket ' + ticket.number;
  });
});