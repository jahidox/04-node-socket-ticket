const TicketControl = require("../models/ticket-control");
const ticketControl = new TicketControl();


const socketController = (socket) => {
    console.log('Cliente conectado', socket.id );

    socket.emit('last-ticket', ticketControl.last);
    socket.emit('actual-state', ticketControl.last_4);
    socket.emit('pending-tickets', ticketControl.tickets.length);

    socket.on('disconnect', () => {
        console.log('Cliente desconectado', socket.id );
    });

    socket.on('next-ticket', (payload, callback) => {
      const next = ticketControl.next();
      socket.broadcast.emit('pending-tickets', ticketControl.tickets.length);
      callback(next);
    });

    socket.on('attend-ticket', ({desktop}, callback) => {
      if(!desktop){
        return callback({
          ok: false,
          msg: 'El escritorio es obligatorio'
        });
      }

      const ticket = ticketControl.attend_ticket(desktop);
      socket.broadcast.emit('actual-state', ticketControl.last_4);
      socket.emit('pending-tickets', ticketControl.tickets.length);
      socket.broadcast.emit('pending-tickets', ticketControl.tickets.length);
      if(!ticket){
        return callback({
          ok: false,
          msg: 'No hay tickets pendientes'
        });
      }else{
        return callback({
          ok: true,
          ticket
        });
      }

    });

}

module.exports = {
    socketController
}

